﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace TrieTree
{
    public class Tree
    {
        public Node Root { get; set; } = new Node();
        public void Add(string value)
        {
            Root.Add(value.ToLower());
        }
        public void PrintToTreeView(TreeView treeView)
        {
            if (Root != null)
            {
                treeView.Nodes.Add("");
                Root.PrintToTreeNode(treeView.Nodes[0]);
            }
        }      
        public List<string> Find(string str, string st)
        {

            return Root.Find(str, st);            
        }
        public bool Delete(string word)
        {
            if (Root != null)
            {
                if (Root.Delete(word.ToLower()))
                {
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }
    }
}
