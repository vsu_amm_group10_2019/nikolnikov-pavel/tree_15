﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TrieTree
{
    public partial class Form1 : Form
    {
        Tree Tree = new Tree();
        
        public Form1()
        {
            InitializeComponent();
        }

        private void Uslovie_Click(object sender, EventArgs e)
        {
            MessageBox.Show("В Trie-дереве найти все слова, содержащие только символы заданного множества.",
                            "Условие задачи",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
        }
        private void Redraw()
        {
            treeView1.Nodes.Clear();
            Tree.PrintToTreeView(treeView1);
            treeView1.ExpandAll();
        }

        private void OpenFile_Click_1(object sender, EventArgs e)
        {
            Tree = new Tree();
            openFileDialog.ShowDialog();
            string fileName = openFileDialog.FileName;
            string[] lines = File.ReadAllLines(fileName);
            foreach (string str in lines)
            {
                string[] words = str.Split(' ');
                foreach (string word in words)
                {
                    string tmp = word.Trim();
                    if (!string.IsNullOrEmpty(tmp))
                    {
                        Tree.Add(word);
                    }
                }
            }
            Redraw();
        }
        private void add_Click(object sender, EventArgs e)
        {
            FindForm findForm = new FindForm(FormState.ADD);
            findForm.ShowDialog();
            if (findForm.Ok)
            {
                Tree.Add(findForm.txt);
                Redraw();
            }
        }
        private void Search_in_Tree_Click(object sender, EventArgs e)
        {            
            FindForm findForm = new FindForm(FormState.SEARCH);
            findForm.ShowDialog();
            if (findForm.Ok)
            {
                List<string> result = Tree.Find(findForm.txt, "");
                Result resh = new Result(result);
                resh.ShowDialog();
            }
        }
        
        private void Del_word_Click(object sender, EventArgs e)
        {           
            FindForm findForm = new FindForm(FormState.DELETE);
            findForm.ShowDialog();
            if (findForm.Ok)
            {
                if (!Tree.Delete(findForm.txt))
                {
                    Redraw();
                }
                else
                {
                    MessageBox.Show("Не найдено");
                }
            }

        }
    }
}
