﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrieTree
{
    public class Node
    {
        public Dictionary<char, Node> Childs { get; set; } = new Dictionary<char, Node>();
        public int Count { get; set; }
        public void Add(string value)
        {
            if (value.Length == 0)
            {
                Count++;
                return;
            }
            char next = value[0];
            string remaining = value.Length == 1 ? "" : value.Substring(1);

            if (!Childs.ContainsKey(next))
            {
                Node node = new Node();
                Childs.Add(next, node);
            }
            Childs[next].Add(remaining);
        }

        public void PrintToTreeNode(TreeNode treeNode)
        {
            int i = 0;
            foreach (var keyValuePair in Childs)
            {
                treeNode.Nodes.Add(keyValuePair.Key.ToString());
                keyValuePair.Value.PrintToTreeNode(treeNode.Nodes[i]);
                i++;
            }
        }

        public bool Delete(string word)
        {
            if (word == "")
            {
                if (Childs.Count != 0)
                {
                    Count = 0;
                    return false;
                }
                return true;
            }
            else
            {
                char next = word[0];
                string remaining = word.Length == 1 ? "" : word.Substring(1);

                if (Childs.ContainsKey(next))
                {
                    if (Childs[next].Delete(remaining))
                    {
                        Childs.Remove(next);
                        if (Childs.Count > 0)
                        {
                            return false;
                        }
                        return true;
                    }
                }
            }
            return false;
        }

        public List<string> Find(string allowedSymbols, string start)
        {
            List<string> result = new List<string>();
            if (Count > 0)
            {
                result.Add(start);
            }
            char[] charArr = allowedSymbols.ToCharArray();
            foreach (char ch in charArr)
            {
                if (Childs.ContainsKey(ch))
                {
                    result.AddRange(Childs[ch].Find(allowedSymbols, start + ch.ToString()));
                }
            }
            return result;
        } 
    }
}
