﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrieTree
{
    public partial class Result : Form
    {
        public Result( List<string> data)
        {
            InitializeComponent();            
            dataGridView1.RowCount = data.Count;
            int i = 0;
            foreach (var elem in data)
            {
                dataGridView1.Rows[i].Cells[0].Value = elem;
                i++;
            }
        }
    }
}
