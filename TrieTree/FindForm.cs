﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrieTree
{
    public partial class FindForm : Form
    {
        Tree TreeF;
        FormState FormState;
        public string txt;
        public bool Ok { get; set; } = false;
        public FindForm(FormState formState)
        {
            InitializeComponent();
            FormState = formState;
            switch (formState)
            {
                case FormState.ADD:
                    {
                        label1.Text = "Введите слово:";
                        btn.Text ="Добавить";
                        break;
                    }
                case FormState.DELETE:
                    {
                        label1.Text = "Введите слово:";
                        btn.Text ="Удалить";
                        break;
                    }
                case FormState.SEARCH:
                    {
                        label1.Text = "Введите множество букв:";
                        btn.Text ="Найти";
                        break;
                    }
            }
        }
      
        private void NameAdd_KeyPress(object sender, KeyPressEventArgs e)
        { //принимаем только буквы
            char letter = e.KeyChar;
            if (!Char.IsLetter(letter) && e.KeyChar != (char)Keys.Back && e.KeyChar != (char)Keys.Delete)
            {
                e.Handled = true;
            }
        }
        public string creat_str(string value)
        {//создание множества
            string s, mnoj;
            int i;
            mnoj = "";
            s = value.ToLower();
            for (i = 0; i < s.Length; i++)
            {
                if (!mnoj.Contains(s[i]))
                {
                    mnoj += s[i];
                }
            }
            return mnoj;
        }

        private void btn_Click(object sender, EventArgs e)
        {
            if (FormState == FormState.SEARCH)
            {
                if (string.IsNullOrEmpty(textBox1.Text))
                {
                    textBox1.BackColor = Color.FromArgb(255, 40, 60);
                    MessageBox.Show("Введите множество букв",
                                "Ошибка",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);

                }
                else
                {
                    textBox1.BackColor = Color.White;
                    txt = creat_str(textBox1.Text);                     
                    Ok = true;
                    Close();

                }
            }
            else
                if (FormState == FormState.DELETE | FormState == FormState.ADD)
                {
                if (string.IsNullOrEmpty(textBox1.Text))
                {
                    textBox1.BackColor = Color.FromArgb(255, 40, 60);
                    MessageBox.Show("Введите слово",
                                "Ошибка",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);

                }
                else
                {
                    textBox1.BackColor = Color.White;
                    txt = textBox1.Text;
                    Ok = true;
                    Close();
                }
            }
        }
            
    }
}
