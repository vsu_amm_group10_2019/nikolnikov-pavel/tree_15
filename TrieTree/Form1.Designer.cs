﻿
namespace TrieTree
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.OpenFile = new System.Windows.Forms.ToolStripMenuItem();
            this.действиеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.найтиСловаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.добавитьСловоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьСловоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Uslovie = new System.Windows.Forms.ToolStripMenuItem();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpenFile,
            this.действиеToolStripMenuItem,
            this.Uslovie});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(586, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // OpenFile
            // 
            this.OpenFile.Name = "OpenFile";
            this.OpenFile.Size = new System.Drawing.Size(66, 20);
            this.OpenFile.Text = "Открыть";
            this.OpenFile.Click += new System.EventHandler(this.OpenFile_Click_1);
            // 
            // действиеToolStripMenuItem
            // 
            this.действиеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.найтиСловаToolStripMenuItem,
            this.добавитьСловоToolStripMenuItem,
            this.удалитьСловоToolStripMenuItem});
            this.действиеToolStripMenuItem.Name = "действиеToolStripMenuItem";
            this.действиеToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.действиеToolStripMenuItem.Text = "Действие";
            // 
            // найтиСловаToolStripMenuItem
            // 
            this.найтиСловаToolStripMenuItem.Name = "найтиСловаToolStripMenuItem";
            this.найтиСловаToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.найтиСловаToolStripMenuItem.Text = "Найти слова";
            this.найтиСловаToolStripMenuItem.Click += new System.EventHandler(this.Search_in_Tree_Click);
            // 
            // добавитьСловоToolStripMenuItem
            // 
            this.добавитьСловоToolStripMenuItem.Name = "добавитьСловоToolStripMenuItem";
            this.добавитьСловоToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.добавитьСловоToolStripMenuItem.Text = "Добавить слово";
            this.добавитьСловоToolStripMenuItem.Click += new System.EventHandler(this.add_Click);
            // 
            // удалитьСловоToolStripMenuItem
            // 
            this.удалитьСловоToolStripMenuItem.Name = "удалитьСловоToolStripMenuItem";
            this.удалитьСловоToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.удалитьСловоToolStripMenuItem.Text = "Удалить слово";
            this.удалитьСловоToolStripMenuItem.Click += new System.EventHandler(this.Del_word_Click);
            // 
            // Uslovie
            // 
            this.Uslovie.Name = "Uslovie";
            this.Uslovie.Size = new System.Drawing.Size(65, 20);
            this.Uslovie.Text = "Условие";
            this.Uslovie.Click += new System.EventHandler(this.Uslovie_Click);
            // 
            // treeView1
            // 
            this.treeView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeView1.Location = new System.Drawing.Point(0, 27);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(586, 516);
            this.treeView1.TabIndex = 1;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Info;
            this.ClientSize = new System.Drawing.Size(586, 543);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(300, 200);
            this.Name = "Form1";
            this.Text = "Задача 15";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem OpenFile;
        private System.Windows.Forms.ToolStripMenuItem Uslovie;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ToolStripMenuItem действиеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem найтиСловаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem добавитьСловоToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалитьСловоToolStripMenuItem;
    }
}

